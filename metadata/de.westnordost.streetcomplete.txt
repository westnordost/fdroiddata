Categories:Navigation
License:GPL-3.0
Author Name:Tobias Zwick
Author Email:osm@westnordost.de
Web Site:https://github.com/westnordost/StreetComplete
Source Code:https://github.com/westnordost/StreetComplete
Issue Tracker:https://github.com/westnordost/StreetComplete/issues

Auto Name:StreetComplete
Summary:OpenStreetMap surveyor app
Description:
Help to improve the OpenStreetMap with StreetComplete!

This app finds incomplete and extendable data in your vicinity and displays it
on a map as markers. Each of those is solvable by answering a simple question to
complete the info on site.

The info you enter is then directly added to the OpenStreetMap in your name,
without the need to use another editor.
.

Repo Type:git
Repo:https://github.com/westnordost/StreetComplete.git

Build:0.4,1
    commit=v0.4
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
